// index.ts 2023/5/18 @wanghaoran

import Mock from "mockjs";
import moment from "moment";

Mock.Random.extend({
    czzdw: function(date) {
        // var constellations = ['白羊座', '金牛座', '双子座', '巨蟹座', '狮子座', '处女座', '天秤座', '天蝎座', '射手座', '摩羯座', '水瓶座', '双鱼座']
        return `采油管理${Mock.Random.cword('零一二三四五六七八九十')}区注采${Mock.Random.integer(50,150)}站`
    },
    getDatesBetween: function(startDate, endDate, dateFormat) {
        let dates = [];
        let currentDate = new Date(startDate);
        let end = new Date(endDate);
        while (currentDate <= end) {
            dates.push(moment(currentDate).format(dateFormat));
            currentDate.setDate(currentDate.getDate() + 1);
        }
        return dates;
    }
})

export default Mock;
