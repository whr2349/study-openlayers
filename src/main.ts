import { createApp } from "vue";
import App from "./App.vue";
import { router } from "./router";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// import vform from "vform"
// import 'vform/dist/designer.style.css'
// @ts-ignore
import vform from "whr-form-v3/designer.umd.js"
import 'whr-form-v3/designer.style.css'
import './common/css/index.scss'
import VueFullscreen from 'vue-fullscreen'
import appCofnfig from "./utils/config";

appCofnfig.aaa = 'uuu';
const app = createApp(App);
app.use(ElementPlus);
app.use(vform)
app.use(VueFullscreen)
// for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
//   app.component(key, component);
// }
app.use(router).mount("#app");
