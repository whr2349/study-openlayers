import {createRouter, createWebHashHistory} from "vue-router";
import layout from "../layout/index.vue"
import SvgFirst from "../pages/svg/SvgFirst.vue";

const routes = [{
    path: "/",
    component: layout,
    redirect: "Openlayers/AccessibleMap",
    children: [
        {
            path: "page404",
            name: "page404",
            meta: {title: "page404", ismenu: false, keepalive: false, router: '/page404'},
            component: () => import("../pages/page404.vue")
        },
        {
            path: "Openlayers",
            name: "Openlayers",
            meta: {title: "Openlayers", ismenu: true, keepalive: false, router: '/Openlayers'},
            children: [
                {
                    path: "AccessibleMap",
                    name: "AccessibleMap",
                    meta: {title: "第一个例子", ismenu: true, keepalive: false, router: 'Openlayers/AccessibleMap'},
                    component: () => import("../pages/AccessibleMap.vue")
                },
                {
                    path: "tianditu",
                    name: "tianditu",
                    meta: {title: "加载天地图", ismenu: true, keepalive: false, router: 'Openlayers/tianditu'},
                    component: () => import("../pages/tianditu.vue")
                },
            ]
        },
        {
            path: "VueUsage",
            name: "VueUsage",
            meta: {title: "vue用法", ismenu: true, keepalive: false, router: '/VueUsage'},
            children: [
                {
                    path: "baohuo",
                    name: "baohuo",
                    meta: {title: "保活组件", ismenu: true, keepalive: true, router: 'VueUsage/baohuo'},
                    component: () => import("../pages/baohuo.vue")
                },
                {
                    path: "VueInstance",
                    name: "VueInstance",
                    meta: {title: "vue实例", ismenu: true, keepalive: false, router: 'VueUsage/VueInstance'},
                    component: () => import("../pages/vue/VueInstance.js")
                },
            ]
        },
        {
            path: "VueComponentLibraries",
            name: "VueComponentLibraries",
            meta: {title: "三方组件库", ismenu: true, keepalive: false, router: '/VueComponentLibraries'},
            children: [
                {
                    path: "vform",
                    name: "vform",
                    meta: {title: "vform", ismenu: true, keepalive: false, router: 'VueComponentLibraries/vform'},
                    component: () => import("../pages/vform.vue")
                },
                {
                    path: "aceEditor",
                    name: "aceEditor",
                    meta: {
                        title: "aceEditor",
                        ismenu: true,
                        keepalive: false,
                        router: 'VueComponentLibraries/aceEditor'
                    },
                    // @ts-ignore
                    component: () => import("../pages/aceEditor.tsx")
                },
                {
                    path: "vformSfc",
                    name: "vformSfc",
                    meta: {title: "vformSfc", ismenu: true, keepalive: false, router: 'VueComponentLibraries/vformSfc'},
                    component: () => import("../pages/vformSfc.vue")
                },
                {
                    path: "vue-cropper",
                    name: "vue-cropper",
                    meta: {
                        title: "vue-cropper",
                        ismenu: true,
                        keepalive: false,
                        router: 'VueComponentLibraries/vue-cropper'
                    },
                    component: () => import("../pages/vue-cropper.vue")
                },
                {
                    path: "element-plus",
                    name: "element-plus",
                    meta: {
                        title: "element-plus",
                        ismenu: true,
                        keepalive: false,
                        router: 'VueComponentLibraries/element-plus'
                    },
                    component: () => import("../pages/element-plus/index.vue")
                },
            ]
        },
        {
            path: "css",
            name: "css",
            meta: {title: "css相关", ismenu: true, keepalive: false, router: '/css'},
            children: [
                {
                    path: "grid-layout",
                    name: "grid-layout",
                    meta: {title: "grid布局", ismenu: true, keepalive: false, router: 'css/grid-layout'},
                    component: () => import("../pages/grid-layout/index.vue")
                },
            ]
        },
        {
            path: "chart",
            name: "chart",
            meta: {title: "图表", ismenu: true, keepalive: false, router: '/chart'},
            children: [
                {
                    path: "echart",
                    name: "echart",
                    meta: {title: "echart", ismenu: true, keepalive: false, router: 'chart/echart'},
                    component: () => import("../pages/echart/index.vue")
                },
                {
                    path: "cuboidBarChart",
                    name: "cuboidBarChart",
                    meta: {title: "方块立体柱状图", ismenu: true, keepalive: false, router: 'chart/echart'},
                    component: () => import("../pages/echart/cuboidBarChart.vue")
                },
                {
                    path: "lineChart",
                    name: "lineChart",
                    meta: {title: "折线图", ismenu: true, keepalive: false, router: 'chart/echart'},
                    component: () => import("../pages/echart/lineChart.vue")
                },
                {
                    path: "concentricCircleChart",
                    name: "concentricCircleChart",
                    meta: {title: "多圆环嵌套图", ismenu: true, keepalive: false, router: 'chart/echart'},
                    component: () => import("../pages/echart/concentricCircleChart.vue")
                },
            ]
        },
        {
            path: "Favoritepage",
            name: "Favoritepage",
            meta: {title: "收藏的页面", ismenu: true, keepalive: false, router: '/Favoritepage'},
            children: [
                {
                    path: "homepage-changji",
                    name: "homepage-changji",
                    meta: {title: "homepage-changji", ismenu: true, keepalive: false, router: 'Favoritepage/homepage-changji'},
                    component: () => import("../pages/Favoritepage/home/homepage-changji.vue")
                },
                {
                    path: "designer",
                    name: "designer",
                    meta: {title: "简单的设计器", ismenu: true, keepalive: false, router: 'Favoritepage/designer'},
                    component: () => import("../pages/Favoritepage/designer/index.vue")
                },
            ]
        },
        {
            path: "VueSvg",
            name: "VueSvg",
            meta: {title: "工艺图Svg", ismenu: true, keepalive: false, router: '/VueSvg'},
            children: [
                {
                    path: "SvgFirst",
                    name: "SvgFirst",
                    meta: {title: "2号柔性井", ismenu: true, keepalive: false, router: '/VueSvg/SvgFirst'},
                    component: SvgFirst
                },
            ]
        },
    ]
}];
const router = createRouter({
    // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
    history: createWebHashHistory(),
    routes, // `routes: routes` 的缩写
});

// router 拦截器，如果没有匹配的路由则跳转404
router.beforeEach(async (to, from) => {
    console.log(to)
    if (!router.hasRoute(to.name ?? '')) {
        // 如果路由名称不存在，则重定向到 404 页面
        return {name: 'page404'}
    }
});

export {
    router,
    routes
}
