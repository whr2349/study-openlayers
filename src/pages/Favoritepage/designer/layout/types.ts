export interface BaseComponent {
  id: string;
  name: string;
  type: string;
  x: number;
  y: number;
  width?: number;
  height?: number;
}

export interface TextComponent extends BaseComponent {
  type: "text";
  content: string;
  fontSize: number;
  color?: string;
  fontWeight?: string;
}

export interface ImageComponent extends BaseComponent {
  type: "image";
  url: string;
}

export interface ButtonComponent extends BaseComponent {
  type: "button";
  content: string;
  buttonType?: string;
}

export interface InputComponent extends BaseComponent {
  type: "input";
  placeholder: string;
  value: string;
}

export type DesignerComponent =
  | TextComponent
  | ImageComponent
  | ButtonComponent
  | InputComponent;
