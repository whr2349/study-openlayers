import {createApp, defineComponent, onMounted, ref, App, ComponentPublicInstance, VueElement,
    Ref, computed} from 'vue'

export default defineComponent({
    setup: () => {
        const htmlref: Ref = ref(null)
        let elRef: Ref = ref(null)

        // 判断对象类型
        function isVueInstance(obj: any) {
            if (obj.$el) {
                console.log('有el', obj.$el)
            } else {
                console.log('dom')
            }
        }

        onMounted(() => {
            console.log(htmlref.value)
            console.log(elRef.value)
            isVueInstance(htmlref.value)
            isVueInstance(elRef.value)
        })

        const hiddenDom = () => {
            htmlref.value.style.display = "none"
        }
        const hiddenEl = () => {
            console.log(elRef.value)
            console.log(elRef.value.$el.style.display = "none")
        }
        return () => (
            <div>
                <div id="testapp"></div>
                <div ref={htmlref}>htmlTemplate</div>
                <el-button ref={elRef}>elTemplate</el-button>
                <el-button onClick={hiddenDom}>隐藏dom</el-button>
                <el-button onClick={hiddenEl}>隐藏el</el-button>
            </div>
        )
    }
})
