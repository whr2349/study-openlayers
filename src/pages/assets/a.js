//车
class Vehicle {
  constructor(label, spend) {
    this.label = label;
    this.spend = spend;
  }
  comput(num, day) {
    return this.spend * num * day;
  }
}
// 公司有的车的类目
let vehicleList = new Map([
  ["宝马", new Vehicle("宝马", 300)],
  ["辆别克商务舱", new Vehicle("辆别克商务舱", 250)],
  ["辆金龙（34）座", new Vehicle("辆金龙（34）座", 600)],
]);
// 构造一个车
function vehicleFactory(str) {
  return vehicleList.get(str);
}
// 用户选车
let userSelected = [
  {
    label: "宝马",
    num: 2,
    day: 5,
  },
  {
    label: "辆别克商务舱",
    num: 1,
    day: 5,
  },
  {
    label: "辆金龙（34）座",
    num: 1,
    day: 5,
  },
];
// 计算总价
function computeTotalPrice(userSelected) {
  return userSelected.reduce((prev, cur, index) => {
    return prev + vehicleFactory(cur.label).comput(cur.num, cur.day);
  }, 0);
}
console.log(computeTotalPrice(userSelected));

//卡车
class Truck extends Vehicle {
  constructor(label, spend, ton) {
    super(label, spend);
    this.ton = ton;
  }
  comput(num, day) {
    return this.spend * num * day * this.ton;
  }
}
// 新买了一个20吨的卡车
vehicleList.set("卡车", new Truck("卡车", 50, 20));

// 确定让我写数据库查询吗？我有点蒙圈，我用js写个生成树形结构的方法？
// 模拟一个数据
const menu = [
  {
    id: 1,
    pid: null,
    title: "系统管理",
  },
  {
    id: 2,
    pid: 1,
    title: "用户管理",
  },
  {
    id: 3,
    pid: 1,
    title: "菜单管理",
  },
  {
    id: 4,
    pid: 1,
    title: "权限管理",
  },
];

function getTree(arr, pid = null) {
  return arr
    .filter((v) => v.pid == pid)
    .map((v) => {
      const children = getTree(arr, v.id);
      if (children.length) {
        v.children = getTree(arr, v.id);
      }
      return v;
    });
}
console.log(getTree(menu));
