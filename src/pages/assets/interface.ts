interface IlayersData {
    id: string;
    label: string;
    url: string;
    projection?: string;
    opacity?: number;
    visible?:boolean,
    children: Array<IlayersData>;
}
interface IxyzParams {
    type: string;
    projection?: string;
    opacity?: number;
    properties?: object;
}
export type {
    IlayersData,IxyzParams
}