import XYZ from "ol/source/XYZ";
import TileLayer from "ol/layer/Tile";
import Map from "ol/Map";
import View from "ol/View";
import LayerGroup from "ol/layer/Group";
import { IlayersData,IxyzParams } from "./interface"
/**
 * 天地图key
 */
const myTDkey = "ab897a6ed1338580758b026190b25e7c";


/**
 * XYZ方式获取天地图
 * @param type
 */
function getTDXYZUrl(type: string) {
    return (
        "http://t" +
        Math.round(Math.random() * 7) +
        ".tianditu.com/DataServer?T=" +
        type +
        "&x={x}&y={y}&l={z}&tk=" +
        myTDkey
    );
}

/**
 * XYZ的方式创建Layer；
 * 使用URL模板中定义的一组XYZ格式的URL为平铺数据图层源。默认情况下，它遵循广泛使用的谷歌网格，其中x 0和y 0位于左上角。
 * 像TMS这样的网格，x 0和y 0在左下角，可以通过在URL模板中使用{-y}占位符来使用，只要源文件没有自定义平铺网格。在这种情况下，
 * 可以使用tileUrlFunction
 * @param params
 */
function crtLayerXYZ(params: IxyzParams) {
    var layer = new TileLayer({
        source: new XYZ({
            url:
                "http://t" +
                Math.round(Math.random() * 7) +
                ".tianditu.com/DataServer?T=" +
                params.type +
                "&x={x}&y={y}&l={z}&tk=" +
                myTDkey,
            // projection: proj
        }),
        opacity: params.opacity,
        properties: params.properties,
    });
    return layer;
}

const layersData = [
    {
        id: "1",
        label: "天地图",
        url: "",
        opacity: 100,
        visible:true,
        children: [
            {
                id: "4",
                label: "矢量注记",
                url: getTDXYZUrl("cva_w"),
                opacity: 100,
                visible:true,
                children: [],
            },
            {
                id: "3",
                label: "矢量底图",
                url: getTDXYZUrl("vec_w"),
                opacity: 80,
                visible:true,
                children: [],
            },

        ],
    },
    {
        id: "2",
        label: "影像底图",
        url: getTDXYZUrl("img_w"),
        opacity: 100,
        visible:true,
        children: [],
    },
];

/**
 * 根据 @{layersData}创建layers
 * @param layersData
 */
function makeTree(layersData: Array<IlayersData>) {
    let rootLayers: (LayerGroup | TileLayer<XYZ>)[] = [];
    layersData.forEach((e) => {
        if (e.children.length > 0) {
            let group = new LayerGroup({
                properties: {
                    label: e.label,
                },
                opacity: e.opacity!/100,
                layers: makeTree(e.children),
                visible:e.visible,
            });
            rootLayers.unshift(group);

        } else {
            let layer = new TileLayer({
                source: new XYZ({
                    url: e.url,
                    projection: e.projection ?? "EPSG:3857",
                }),
                opacity: e.opacity!/100,
                properties: {
                    label: e.label,
                },
            });
            rootLayers.unshift(layer)
        }
    });
    return rootLayers
}

let layers = makeTree(layersData)

/**
 * 初始化地图
 * @param optons target 容器dom
 * @returns 地图instacnc
 */
function initMap(optons: { target: HTMLElement }) {
    return new Map({
        layers: layers,
        target: optons.target as HTMLElement,
        view: new View({
            center: [0, 0],
            zoom: 2,
        }),
    });
}
export {
    initMap,layersData
}

