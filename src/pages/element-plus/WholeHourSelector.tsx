// 整点选择器，选择双数

import {defineComponent, ref, useAttrs, watch} from "vue";
import moment from "moment";

export default defineComponent({
    name: "WholeHourSelector",
    setup: (props, {emit}) => {
        const attrs = useAttrs()
        const value = ref('')
        console.log(attrs)

        function getDoubleDigitHourArray() {
            const hoursArray = [];

            for (let hours = 0; hours < 24; hours += 2) {
                const doubleDigitHour = hours.toString().padStart(2, '0');
                hoursArray.push(doubleDigitHour + ':00');
            }

            return hoursArray;
        }

        // 调用方法获取双数整点时间的数组
        const hourList = getDoubleDigitHourArray().map(hour => {
            return {
                name: hour,
                value: hour
            }
        });
        console.log(hourList);
        watch(value, () => {

            console.log(value)
            emit('update:hour', value)
        }, {immediate: true})
        return () => (
            <el-select {...attrs} v-model={value.value}>
                {
                    hourList.map((item, index) => {
                        return (<el-option lable={item.name} value={item.value}
                                           key={item.value}></el-option>)
                    })
                }
            </el-select>
        )
    }
})