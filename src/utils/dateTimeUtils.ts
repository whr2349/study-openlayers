// dateTimeUtils.js 2023/5/17 @wanghaoran

import moment from "moment";

/**
 * 获取 startDate 和 endDate 之前的日期
 * @param startDate - 起始时间
 * @param endDate - 结束时间
 * @param dateFormat - 格式
 * @returns {*[]}
 */
const getDatesBetween = function(startDate:string, endDate:string, dateFormat = 'YYYY-MM-DD') {
    let dates = [];
    let currentDate = new Date(startDate);
    let end = new Date(endDate);
    while (currentDate <= end) {
        dates.push(moment(currentDate).format(dateFormat));
        currentDate.setDate(currentDate.getDate() + 1);
    }
    return dates;
}
