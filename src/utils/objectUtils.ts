// objectUtils.js 2023/5/18 @wanghaoran

/**
 * 用于深度合并两个对象.
 * target 是要合并到的目标对象，source 是要合并的源对象
 * 当合并的属性是函数时，会检查目标对象和源对象中对应的属性是否都是函数。如果是，它将创建一个新的函数，该函数会依次调用目标对象的方法和源对象的方法。这样，合并后的对象中的方法将包含两个对象的方法逻辑。
 * @param target - 是要合并到的目标对象
 * @param source - 是要合并的源对象
 * @returns {*}
 */
export function deepMergeObjects(target:Record<any, any>, source:Record<any, any>) {
    for (let key in source) {
        if (source.hasOwnProperty(key)) {
            if (source[key] instanceof Object && key in target && target[key] instanceof Object) {
                if (typeof source[key] === 'function' && typeof target[key] === 'function') {
                    target[key] = function () {
                        target[key].apply(this, arguments);
                        source[key].apply(this, arguments);
                    };
                } else {
                    deepMergeObjects(target[key], source[key]);
                }
            } else {
                target[key] = source[key];
            }
        }
    }
    return target;
}

/**
 * 输入初始颜色 "rgba(29,67,243,1)" 生成一组逐渐变浅的一组颜色
 * @param color
 */
export function generateLighterColors(color: string): string[] {
    let r: number, g: number, b: number, a: number;

    if (color.startsWith("#")) {
        color = color.slice(1);
        if (color.length === 3) {
            color = color
                .split("")
                .map((c) => c + c)
                .join("");
        }

        r = parseInt(color.substr(0, 2), 16);
        g = parseInt(color.substr(2, 2), 16);
        b = parseInt(color.substr(4, 2), 16);
    } else if (color.startsWith("rgba")) {
        const rgba = color.match(/\d+(\.\d+)?/g);
        r = parseInt(rgba![0], 10);
        g = parseInt(rgba![1], 10);
        b = parseInt(rgba![2], 10);
        a = parseFloat(rgba![3]);
    } else {
        throw new Error("Invalid color format");
    }

    const lighterColors: string[] = [];

    for (let i = 0; i < 7; i++) {
        const factor = 1 + i * 0.2; // 调整因子，用于控制颜色变浅程度
        const newR = Math.round(r * factor);
        const newG = Math.round(g * factor);
        const newB = Math.round(b * factor);

        // @ts-ignore
        const rgba = a !== undefined ? `rgba(${newR}, ${newG}, ${newB}, ${a * (1 - i * 0.15)})` : `rgba(${newR}, ${newG}, ${newB})`;
        lighterColors.push(rgba);
    }

    return lighterColors;
}

/**
 * 接受一个函数,获取形参与实参组成的 key value 对象
 * function exampleFunc(a, b, c) {
 *     // console.log(a,b,c)
 * }
 *
 *   const paramsObject = getParamsObject(exampleFunc, 1, 'two', true);
 *   console.log(paramsObject);
 *   {a: 1, b: 'two', c: true}
 * @param func
 */
export function getParamsObject(func:Function) {
    const paramNames = [...func.toString().matchAll(/\((.*?)\)/gs)][0][1]
        .split(',')
        .map((param) => param.trim());
    const paramValues = [...arguments].slice(1);
    return Object.fromEntries(paramNames.map((name, index) => [name, paramValues[index]]));
}





