import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import * as path from "node:path";
// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        vue(),
        vueJsx({}),
    ],
    css: {
        modules: {
            scopeBehaviour: 'local',
            localsConvention: 'camelCase'
        }
    },
    // 设置快捷路径@为绝对路径src
    resolve: {
        alias: {
            '@': path.resolve(__dirname, 'src')
        }
    },

})
